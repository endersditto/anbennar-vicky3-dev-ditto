egduqon_completion_trigger = {
	AND = {
		ig:ig_rural_folk = {
			is_in_government = no
		}
		OR = {
			government_legitimacy >= 50
			ig:ig_rural_folk = { is_marginal = yes }
		}
		has_insurrectionary_interest_groups = no
		is_at_war = no		
	}
}