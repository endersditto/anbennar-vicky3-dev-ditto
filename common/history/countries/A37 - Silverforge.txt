﻿COUNTRIES = {
	c:A37 ?= {
		effect_starting_technology_tier_2_tech = yes
		
		#From Anbennar laws
		activate_law = law_type:law_parliamentary_republic
		activate_law = law_type:law_oligarchy
		activate_law = law_type:law_racial_segregation
		activate_law = law_type:law_freedom_of_conscience
		activate_law = law_type:law_appointed_bureaucrats
		activate_law = law_type:law_professional_army

		activate_law = law_type:law_interventionism
		activate_law = law_type:law_mercantilism
		activate_law = law_type:law_per_capita_based_taxation
		activate_law = law_type:law_tenant_farmers
		activate_law = law_type:law_local_police
		activate_law = law_type:law_charitable_health_system
		
		activate_law = law_type:law_censorship
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_women_own_property
		activate_law = law_type:law_legacy_slavery
		
		activate_law = law_type:law_local_tolerance
		
		
		activate_law = law_type:law_nation_of_magic
		
	}
}