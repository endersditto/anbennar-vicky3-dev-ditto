﻿CHARACTERS = {
	c:B29 ?= {
		create_character = {
			first_name = Morvel
			last_name = Vyrekynn
			historical = yes
			ruler = yes
			female = yes
			birth_date = 1785.3.05
			is_general = yes
			interest_group = ig_landowners
			ideology = ideology_moderate
			dna = dna_morvel_vyrekynn
			traits = {
				brave charismatic celebrity_commander
			}
		}
		# create_character = {
		# 	first_name = Calrodiy
		# 	last_name = Vyrekynn
		# 	historical = yes
		# 	heir =  yes
		# 	age = 1
		# 	interest_group = ig_petty_bourgeoisie
		# 	ideology = ideology_land_reformer
		# 	traits = {
		# 		honorable romantic sickly
		# 	}
		# }
		create_character = {
			first_name = Adrjon
			last_name = yen_Stanyr
			historical = yes
			age = 25
			ig_leader = yes
			is_general = yes
			interest_group = ig_landowners
			ideology = ideology_royalist
			traits = {
				charismatic cautious honorable
			}
		}
		create_character = {
			first_name = Acerad
			last_name = yen_Stenur
			historical = yes
			age = 36
			is_general = yes
			interest_group = ig_landowners
			ideology = ideology_authoritarian
			traits = {
				meticulous forest_commander
			}
		}
		create_character = {
			first_name = Munaran
			last_name = yen_Cestor
			historical = yes
			age = 42
			ig_leader = yes
			is_general = yes
			interest_group = ig_armed_forces
			ideology = ideology_authoritarian
			traits = {
				grifter imposing ambitious
			}
		}
		create_character = { #grand priest of Adbrabohvi
			first_name = Devynn
			last_name = yen_Bohvi
			historical = yes
			age = 42
			ig_leader = yes
			interest_group = ig_devout
			ideology = ideology_traditionalist
			traits = {
				imperious innovative
			}
		}
	}
}
