﻿POPULATION = {
	# c:B19 = {	#Cestirmark
		
	# 	effect_starting_pop_wealth_high = yes
	# 	effect_starting_pop_literacy_high = yes
		
	# }

	# c:B20 = {	#Marlliande
		
	# 	effect_starting_pop_wealth_very_high = yes #Not as impactful as it seems due to the huge amount of slaves
	# 	effect_starting_pop_literacy_middling = yes
		
	# }

	c:B21 = {	#Ynnsmouth
		
		effect_starting_pop_wealth_medium = yes
		effect_starting_pop_literacy_high = yes
		
	}

	# c:B22 = {	#Zanlib
		
	# 	effect_starting_pop_wealth_high = yes
	# 	effect_starting_pop_literacy_high = yes
		
	# }

	# c:B23 = {	#Isobelin
		
	# 	effect_starting_pop_wealth_high = yes
	# 	effect_starting_pop_literacy_high = yes
		
	# }

	# c:B24 = {	#Thilvis
		
	# 	effect_starting_pop_wealth_high = yes
	# 	effect_starting_pop_literacy_high = yes
		
	# }

	# c:B25 = {	#Valorpoint
		
	# 	effect_starting_pop_wealth_high = yes
	# 	effect_starting_pop_literacy_high = yes
		
	# }

	c:B89 = {	#Daxwell
		
		effect_starting_pop_wealth_medium = yes
		effect_starting_pop_literacy_low = yes
		
	}

	c:B17 = {	#Aelnar

		effect_starting_pop_wealth_medium = yes
		effect_starting_pop_literacy_low = yes

	}

	c:B18 = {	#New Redglades
		
		effect_starting_pop_wealth_medium = yes
		effect_starting_pop_literacy_middling = yes
		
	}

	c:B26= {	#Nur Ionnidar
		
		effect_starting_pop_wealth_medium = yes
		effect_starting_pop_literacy_low = yes
		
	}

	c:B27 = {	#Neratica
		
		effect_starting_pop_wealth_medium = yes
		effect_starting_pop_literacy_middling = yes
		
	}

	c:B98 = {	#Trollsbay

		effect_starting_pop_wealth_high = yes
		effect_starting_pop_literacy_high = yes
		
	}
}