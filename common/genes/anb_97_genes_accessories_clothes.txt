﻿#ANBENNAR CLOTHES

special_genes = {
	accessory_genes = {

    #####################################
	#									#
	# 			   LEGEWEAR			    #
	#									#
	#####################################

		anbennar_legwear = {
			inheritable = no
			
			all_cloths = {
				index = 0
				male = {
					1 = male_gnome_cloth_pants
				}

				female = {
					1 = empty
				}
				boy = male
				girl = female
			}		

			no_cloths = {
				index = 1
				male = {
					1 = empty
				}		   
				female = male
				boy = male
				girl = male
			}	 

			gnome_common_cloths = {
				index = 2
				male = {
					1 = male_gnome_cloth_pants
                }

				female = {
				}
				boy = male
				girl = female
            }
        }
		
    #####################################
	#									#
	# 			   SHIRTS			    #
	#									#
	#####################################

		anbennar_shirts = {
			inheritable = no
			
			all_cloths = {
				index = 0
				male = {
					1 = male_gnome_cloth_shirt
				}

				female = {
					1 = empty
				}
				boy = male
				girl = female
			}		

			no_cloths = {
				index = 1
				male = {
					1 = empty
				}		   
				female = male
				boy = male
				girl = male
			}	 

			gnome_common_shirt = {
				index = 2
				male = {
					1 = male_gnome_cloth_shirt
                }

				female = {
				    1 = female_outfit_polynesian_01_high
				}
				boy = male
				girl = female
			}	 
        }
		
    #####################################
	#									#
	# 			   NECKLECES			#
	#									#
	#####################################

		anbennar_neckleces = {
			inheritable = no
			
			all_neckleces = {
				index = 0
				male = {
					1 = male_gnome_cloth_scarf
				}

				female = {
					1 = empty
				}
				boy = male
				girl = female
			}		

			no_cloths = {
				index = 1
				male = {
					1 = empty
				}		   
				female = male
				boy = male
				girl = male
			}	 

			gnome_common_scarf = {
				index = 2
				male = {
					1 = male_gnome_cloth_scarf
                }

				female = {
				}
				boy = male
				girl = female
			}
}

