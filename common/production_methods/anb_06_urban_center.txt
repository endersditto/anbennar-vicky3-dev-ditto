﻿## Urban Centers
pm_arcane_state_clergy = {
	texture = "gfx/interface/icons/production_method_icons/ownership_clergy.dds"

	unlocking_laws = {
		law_state_religion
	}
	disallowing_laws = {
		law_traditional_magic_banned
	}

	building_modifiers = {
		level_scaled = {
			building_employment_clergymen_add = 140
			building_employment_mages_add = 60
		}
	}
	country_modifiers = {
		workforce_scaled = {
			country_magical_expertise_add = 1
		}
	}
}

pm_arcane_free_urban_clergy = {
	texture = "gfx/interface/icons/production_method_icons/ownership_bureacrats.dds"

	unlocking_laws = {
		law_freedom_of_conscience
		law_total_separation
	}
	disallowing_laws = {
		law_traditional_magic_banned
	}


	building_modifiers = {
		level_scaled = {
			building_employment_clerks_add = 100
			building_employment_clergymen_add = 105
			building_employment_mages_add = 45
		}
	}
	country_modifiers = {
		workforce_scaled = {
			country_magical_expertise_add = 1
		}
	}
}
## Art Academies
pm_arcane_bard_college = {
	texture = "gfx/interface/icons/production_method_icons/mages_ownership.dds"

	unlocking_technologies = {
		bardic_magical_tradition
	}
	
	disallowing_laws = {
		law_traditional_magic_banned
	}

	country_modifiers = {
		workforce_scaled = {
			country_magical_expertise_add = 5
		}
	}

	building_modifiers = {
		level_scaled = {
			building_employment_academics_add = 1500
			building_employment_mages_add = 500
			building_employment_clerks_add = 2000
			building_employment_laborers_add = 1000		
		}
	}
}

## Power Plants
pm_damestear_reactor = {
	texture = "gfx/interface/icons/production_method_icons/oil_fired_plant.dds"

	unlocking_technologies = {
		the_fifth_fundamental_force
	}

	state_modifiers = {
		workforce_scaled = {
			state_pollution_generation_add = 75
		}
	}

	building_modifiers = {
		workforce_scaled = {
			goods_input_engines_add = 25
			goods_input_damestear_add = 50
			goods_input_porcelain_add = 10
			goods_output_electricity_add = 225
		}

		level_scaled = {
			building_employment_laborers_add = 1000
			building_employment_machinists_add = 1500
			building_employment_engineers_add = 2400
		}
	}
}

pm_automata_laborers_power_plants = {
	texture = "gfx/interface/icons/production_method_icons/automated_bakery.dds"
	
	unlocking_laws = {
		law_sapience_mechanim_compromise
		law_sapience_mechanim_unrecognized
	}
	unlocking_technologies = {
		insyaan_analytical_engine		
	}


	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_automata_add = 6	
		}

		level_scaled = {
			building_employment_laborers_add = -3000
		}
	}
}

pm_automata_machinists_power_plants = {
	texture = "gfx/interface/icons/production_method_icons/automated_bakery.dds"
	
	unlocking_laws = {
		law_sapience_mechanim_compromise
		law_sapience_mechanim_unrecognized
	}
	unlocking_technologies = {
		advanced_mechanim			
	}
	
	

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_automata_add = 10	
		}

		level_scaled = {
			building_employment_laborers_add = -3000
			building_employment_machinists_add = -1750
		}
	}
}

pm_automata_laborers_power_plants_enforced = {
	texture = "gfx/interface/icons/production_method_icons/automated_bakery.dds"
	
	unlocking_laws = {
		law_sapience_mechanim_compromise_enforced
		law_sapience_mechanim_unrecognized_enforced
	}
	is_hidden_when_unavailable = yes
	unlocking_technologies = {
		insyaan_analytical_engine		
	}


	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_automata_add = 7	
		}

		level_scaled = {
			building_employment_laborers_add = -3000
		}
	}
}

pm_automata_machinists_power_plants_enforced = {
	texture = "gfx/interface/icons/production_method_icons/automated_bakery.dds"
	
	unlocking_laws = {
		law_sapience_mechanim_compromise_enforced
		law_sapience_mechanim_unrecognized_enforced
	}
	is_hidden_when_unavailable = yes
	unlocking_technologies = {
		advanced_mechanim			
	}
	
	

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_automata_add = 11	
		}

		level_scaled = {
			building_employment_laborers_add = -3000
			building_employment_machinists_add = -1750
		}
	}
}