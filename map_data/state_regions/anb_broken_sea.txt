﻿STATE_TRITHEMAR = {
    id = 513
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0700F0" "x0700F1" "x0F72F3" "x1A741C" "x1B3D7D" "x1C93E4" "x2A2DF9" "x35839B" "x374BEF" "x393FDF" "x3ABC20" "x3B5C7E" "x3C651C" "x3CBC24" "x3F05E8" "x555B2E" "x6D6334" "x71CAD8" "x78955F" "x7939FB" "x7C41A8" "x858CBF" "x8F517F" "x939F2A" "x977A70" "x9806EE" "xAA6512" "xAC9D76" "xB24BF6" "xBA56AD" "xBB5A4F" "xBB5EAD" "xD23718" "xD81D58" "xD9F500" "xDB29E1" "xE07B7B" "xEB348A" }
    traits = { state_trait_broken_sea_whaling }
    city = "x374bef" #NEW PLACE
    port = "xd81d58" #Random
    farm = "x78955f" #Random
    wood = "x2a2df9" #Random
    arable_land = 80
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_fishing = 8
        bg_logging = 12
        bg_whaling = 6
        bg_iron_mining = 24
        bg_lead_mining = 13
    }
    naval_exit_id = 3112
}
STATE_SILDARBAD = {
    id = 552
    subsistence_building = "building_subsistence_farms"
    provinces = { "x01BF3A" "x0500E8" "x08B647" "x0EAE45" "x234A11" "x350B0F" "x3D3ED8" "x3F5A17" "x434C7E" "x435020" "x47BA2C" "x4DBF0E" "x5D6A63" "x7E8FCE" "x7F712C" "x83D701" "x882F94" "x8A9598" "x8BF39D" "x8D3309" "x94AA52" "x996A84" "xB15A4F" "xB1EAC9" "xBE17B4" "xC3524F" "xD1F604" "xD48CEB" "xD88028" "xDAC04E" "xDAC4ED" "xF6490B" "xFDEBB0" }
    traits = { state_trait_forest_cursed_ones }
    city = "x0500e8" #Sildarbad
    port = "x434c7e" #Random
    farm = "xd1f604" #Random
    wood = "x8d3309" #Random
    arable_land = 40
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_fishing = 4
        bg_logging = 24
        bg_iron_mining = 14
    }
    resource = {
        type = "bg_gold_fields"
        depleted_type = "bg_gold_mining"
        undiscovered_amount = 5
        discover_amount_max = 5
    }
    naval_exit_id = 3113
}
STATE_FARNOR = {
    id = 299
    subsistence_building = "building_subsistence_farms"
    provinces = { "x142AEF" "x14D998" "x24E0C7" "x2B5C94" "x30613B" "x38388D" "x435820" "x445820" "x445C7E" "x456C7E" "x471938" "x55FB49" "x59D654" "x5BAFDC" "x644D92" "x68C13E" "x708011" "x74682C" "x911741" "xA70154" "xA7CAA4" "xA9253D" "xAA1FC4" "xAEE2A5" "xB5E1FE" "xBFD046" "xC356AD" "xC408A8" "xC45A4F" "xC56A4F" "xE3B084" "xE78B63" "xE93407" "xF2BD5E" "xFB18E8" }
    traits = {}
    city = "xc356ad" #Random
    port = "x445820" #Random
    farm = "xc45a4f" #Random
    wood = "x59d654" #Random
    arable_land = 40
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_fishing = 8
        bg_logging = 18
        bg_coal_mining = 20
        bg_iron_mining = 35
    }
    resource = {
        type = "bg_gold_fields"
        depleted_type = "bg_gold_mining"
        undiscovered_amount = 4
        discover_amount_max = 4
    }
    naval_exit_id = 3113
}
STATE_DALAIRRSILD = {
    id = 502
    subsistence_building = "building_subsistence_farms"
    provinces = { "x001AA1" "x05D45E" "x0D35DB" "x1F1FE9" "x264684" "x273D46" "x2D8020" "x2E9020" "x347C7E" "x35FC7E" "x362020" "x362CB9" "x36947E" "x369C7E" "x36E150" "x372C7E" "x39447E" "x39AC20" "x48265B" "x522FE7" "x5F409A" "x633145" "x693145" "x70B700" "x78B616" "x7B8DDF" "x7D8DDF" "x818DDF" "x869250" "x89FE58" "x989414" "xAEA850" "xB2743E" "xB47EAD" "xB5924F" "xB69EAD" "xB736C4" "xB946AD" "xB94A4F" "xC3E78C" "xCCB30D" "xCE90FF" "xD63E8B" "xD7DA53" "xE2617C" "xE82A37" "xEFC180" "xF782AD" "xFA6F4A" }
    traits = { state_trait_hjora_river state_trait_forest_cursed_ones }
    city = "xb94a4f" #Random
    farm = "x39447e" #Random
    wood = "x0d35db" #Random
    arable_land = 40
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 28
        bg_sulfur_mining = 12
        bg_iron_mining = 14
		bg_relics_digsite = 5
    }
}
STATE_HJORDAL = {
    id = 504
    subsistence_building = "building_subsistence_farms"
    provinces = { "x1BC602" "x2C42EF" "x348020" "x34847E" "x358C7E" "x359020" "x3DE150" "x47B201" "x517BD1" "x66409A" "x6D4261" "x6F83AD" "x700614" "x7E1301" "x907631" "x9BF036" "x9E6EF7" "xB4824F" "xB58EAD" "xBE8BB1" "xC91DA7" "xD60BE9" "xD92880" "xDCDA53" "xE222ED" "xE8923D" "xE99E3B" "xEBCAEF" "xEE2A37" "xEE7497" "xFF82AD" }
    traits = { state_trait_hjora_river state_trait_teira_range }
    city = "x348020" #Noo Oddansbay
    port = "xd60be9" #Random
    farm = "xdcda53" #Random
    wood = "x3de150" #Random
    arable_land = 150
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_tobacco_plantations }
    capped_resources = {
        bg_fishing = 8
        bg_logging = 24
        bg_whaling = 4
        bg_iron_mining = 36
        bg_coal_mining = 28
    }
    naval_exit_id = 3109
}
STATE_FLOTTNORD = {
    id = 505
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x17C84F" "x244684" "x32847E" "x358820" "x369820" "x65409A" "x773F30" "x90A764" "x9DB41B" "xB696AD" "xB69A4F" "xC51DA7" "xCA0C83" }
    traits = { state_trait_flottnord_fjords state_trait_teira_range }
    city = "x358820" #Random
    port = "xb696ad" #Random
    farm = "x369820" #Random
    wood = "xb69a4f" #Random
    arable_land = 20
    arable_resources = { bg_livestock_ranches }
    capped_resources = {
        bg_fishing = 8
        bg_logging = 6
        bg_whaling = 6
        bg_iron_mining = 28
    }
    naval_exit_id = 3110
}
STATE_SJAVARRUST = {
    id = 507
    subsistence_building = "building_subsistence_fishing_villages"
    provinces = { "x0C5609" "x15EAE2" "x1C97AE" "x383C7E" "x38FC7E" "x3A4C7E" "x3A547E" "x4012C9" "x500F71" "x5527CD" "x5ED155" "x615E25" "x806333" "xB384FF" "xB6D665" "xB8324F" "xB836AD" "xB83A4F" "xB93EAD" "xBA4EAD" "xBA524F" "xCBC752" "xD7A21E" "xE40539" "xF882AD" "xFBADE7" "x15E1C1" "x18C602" "x2B8B08" "x33947E" "x37247E" "x372820" "x376DC1" "x383020" "x5B54A0" "x8588DF" "x984927" "xB43C83" "xB7224F" "xB72A4F" "xB7F6AD" "xCA6546" "xCE9158" "xD0956F" "xEBC180" }
    traits = { state_trait_broken_sea_whaling state_trait_northern_aelantir_glaciers }
    city = "xb93ead" #Random
    port = "x383c7e" #Random
    farm = "xb83a4f" #Random
    wood = "xb836ad" #Random
    arable_land = 40
    arable_resources = { bg_livestock_ranches }
    capped_resources = {
        bg_fishing = 7
        bg_logging = 13
        bg_whaling = 6
        bg_iron_mining = 8
    }
    resource = {
        type = "bg_gold_fields"
        depleted_type = "bg_gold_mining"
        undiscovered_amount = 3
        discover_amount_max = 3
    }
    naval_exit_id = 3111
}
STATE_NYHOFN = {
    id = 508
    subsistence_building = "building_subsistence_fishing_villages"
    provinces = { "x11D353" "x32AB98" "x3D815B" "x50A652" "x59F99F" "x61D155" "x7E6BFF" "xB784FF" "xBD0969" "xBECF32" "xC25969" "xC2AAA3" "xC53E76" "xCE6546" "xD3A32E" "xD512FA" }
    traits = { state_trait_broken_sea_whaling state_trait_northern_aelantir_glaciers }
    city = "xce6546" #Random
    port = "xb784ff" #Random
    farm = "x11d353" #Random
    wood = "xbecf32" #Random
    arable_land = 20
    arable_resources = { bg_livestock_ranches }
    capped_resources = {
        bg_fishing = 6
        bg_whaling = 4
    }
    naval_exit_id = 3110
}
STATE_DALAIREY_WASTES = {
    id = 509
    subsistence_building = "building_subsistence_fishing_villages"
    provinces = { "x2C2F3C" "x3BEB2E" "x828DDF" "xC99759" "xD2956F" "x6CF948" "x8018A3" "x85E56B" "x8D241F" "x90FE7E" "xB04190" "x3002FF" "x04DB73" "x19A814" "x31C090" "x443E92" "x4B3C7E" "x745CF8" "xB07F50" "xDC67F7" "xF74125" }
    traits = { state_trait_dalairey_wastes } #to prevent early colonization
    city = "xc99759" #Random
    port = "x4b3c7e" #Random
    farm = "xd2956f" #Random
    wood = "x828ddf" #Random
    arable_land = 20
    arable_resources = { bg_livestock_ranches }
    capped_resources = {
        bg_fishing = 5
        bg_whaling = 2
        bg_sulfur_mining = 54
    }
	resource = {
        type = "bg_relics_digsite"
        undiscovered_amount = 10
    }
    naval_exit_id = 3109
}
STATE_FIORGAM = {
    id = 511
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0C547E" "x105609" "x108820" "x1D00F8" "x3282DE" "x618B1C" "x638267" "x7A6731" "x8A56AD" "x9086AD" "x908A4F" "xA0C2BF" "xA874E1" "xB586AD" "xCA11DD" "xD1953D" "xD571A8" }
    traits = {}
    city = "x0c547e" #Random
    port = "x9086ad" #Random
    farm = "x108820" #Random
    wood = "x908a4f" #Random
    arable_land = 20
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_tobacco_plantations }
    capped_resources = {
        bg_fishing = 11
        bg_logging = 4
        bg_whaling = 7
    }
    naval_exit_id = 3101
}
STATE_MITTANWEK = {
    id = 512
    subsistence_building = "building_subsistence_fishing_villages"
    provinces = { "x00821E" "x0A6BE6" "x107763" "x1617DD" "x1F405F" "x2D0CA4" "x2FAE4E" "x309661" "x30E94A" "x347744" "x35D461" "x383820" "x39A420" "x3EDD9A" "x42F2E9" "x5B5500" "x5D0E64" "x672B93" "x690DB5" "x6BB828" "x6CEE91" "x7DF383" "x7E59EB" "x84176B" "x878A7C" "x8848C6" "x894B71" "x8B33D9" "x90C050" "xA5EDD1" "xA7AE88" "xABCC1E" "xB00090" "xB40258" "xC37F66" "xCB424F" "xD4ABBA" "xE70539" "xE76C62" "xE855B1" "xF0CA34" "xF2263F" "xF68D31" "xFD577F" "xFEE834" }
    impassable = { "x0A6BE6" "x1617DD" "x309661" "x30E94A" "x347744" "x3EDD9A" "x42F2E9" "xB00090" "xB40258" "xC37F66" "xCB424F" "xE70539" "xE76C62" "xE855B1" "xF68D31" "xFEE834" "xF0CA34" "x90C050" "x2D0CA4" }
    traits = { state_trait_northern_aelantir_glaciers state_trait_the_machine }
    city = "x894B71" #NEW PLACE
    port = "x2fae4e" #Farplott
    farm = "x35d461" #Arbcyrr
    wood = "xabcc1e" #Inuvuorlak
    mine = "x5D0E64" #Sterngcottopcott
    arable_land = 20
    arable_resources = { bg_livestock_ranches }
    capped_resources = {
        bg_fishing = 4
        bg_whaling = 2
        bg_iron_mining = 8
    }
    resource = {
        type = "bg_gold_fields"
        depleted_type = "bg_gold_mining"
        undiscovered_amount = 4
        discover_amount_max = 4
    }
    naval_exit_id = 3112
}
STATE_FARPLOTT = {
    id = 517
    subsistence_building = "building_subsistence_fishing_villages"
    provinces = { "x0078F4" "x0F1339" "x1A5ED2" "x21A4C8" "x2B18E0" "x31F03F" "x43DAD0" "x4A0914" "x5249FC" "x604D15" "x6D25DC" "x6ED74B" "x788B48" "x807115" "x87DFC3" "x8DF5C5" "x93EF4F" "x97865C" "x9DA2FB" "xA0A058" "xB72711" "xDC5D80" "xF8B9BC" "xFC2A57" }
    impassable = { "x0F1339" "x2B18E0" "x43DAD0" "x4A0914" "x6D25DC" "x788B48" "x807115" "x87DFC3" "x8DF5C5" "x97865C" "xFC2A57" }
    traits = { state_trait_northern_aelantir_glaciers }
    city = "x0078f4" #Random
    port = "x21a4c8" #Random
    farm = "xa0a058" #Random
    wood = "xb72711" #Random
    arable_land = 20
    arable_resources = { bg_livestock_ranches }
    capped_resources = {
        bg_fishing = 3
        bg_logging = 2
        bg_whaling = 2
        bg_iron_mining = 8
    }
    naval_exit_id = 3114
}
STATE_ARSERGOZHAR = {
    id = 280
    subsistence_building = "building_subsistence_farms"
    provinces = { "x017FE3" "x029422" "x073EDA" "x25D0EA" "x457020" "x467820" "x5A2781" "x811259" "x88AB54" "x8C10F7" "x8F7C86" "xA67B4E" "xC18F6B" "xC2F9FC" "xC56EAD" "xE6882E" "xEA960E" "xFC561E" }
    traits = { state_trait_magical_land_lesser }
    city = "x8c10f7" #Random
    farm = "x467820" #Random
    wood = "x457020" #Random
    arable_land = 50
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 18
        bg_coal_mining = 8
        bg_lead_mining = 32
    }
    resource = {
        type = "bg_gold_fields"
        depleted_type = "bg_gold_mining"
        undiscovered_amount = 3
        discover_amount_max = 3
    }
}
